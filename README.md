# Knowledge

_**Constantly In-Progress...**_

In this repository I try and accumulate whatever I know about various things, so as to "take them" with me.

What I mean is that there are many things that we strive to understand, and once we do we just use the results and never look back. Until such time when we need them again and we have to start all over again.

One example that comes to mind is how to install and configure a certain tool, or the sequence of steps needed to setup a webhook, or something like this.

I'll try and divide things in sections.

`bash` instructions will almost always use `yum` instead of `apt`, but you can change that to whatever package manager you love.

[[_TOC_]]

---

## Tools

Various tools I use. Several kinds of tools:

- [DevOps Tools](tools/README.md)
