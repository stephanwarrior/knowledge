# Kubernetes

From the docs:

    Kubernetes (K8s) is an open-source system for automating deployment, scaling, and management of containerized applications.

Kubernetes works on clusters of nodes (which can be physical machines or VM) all orchestrated by a master node.

The way k8s works is by defining _"Resources"_ (with yaml files) and having _"Controllers"_ steer the status of the cluster to the state defined by the resources.

The master exposes a set of REST APIs though which everything is accomplished.

[[_TOC_]]

## K8s Resources

An excellent read about k8s resources and how networking works in kubernetes (which explains a lot about how resources are defined and configured) is this [3-parts series](https://medium.com/google-cloud/understanding-kubernetes-networking-pods-7117dd28727) by Mark Betz.

To create the resources we typically use the `kubectl` command line tool:

```bash
$> kubectl apply -f <path-to-file.yml>
```

To simplify the use of multiple resources we can also use [Helm](helm.md).

### Pods

**Pods** are the basic execution unit of a K8s application, representing processes running on the cluster.

Pods are typically thin wrappers around your containers, with a 1:1 Pod-Container relation. It's also possible to have more than one container in a pod (see [here](https://kubernetes.io/blog/2015/06/the-distributed-system-toolkit-patterns/) for more info on this).

```yml
---
apiVersion: batch/v1
kind: Job
metadata:
  name: hello
spec:
  template:
    spec:
      containers:
      - name: hello
        image: busybox
        command: ['sh', '-c', 'echo "Hello, Kubernetes!" && sleep 3600']
      restartPolicy: OnFailure
```

### Deployments

Usually pods aren't used directly, and instead they're created by the system through the use of other Resource types.

The most common is a **Deployment**.

A Deployment is a wrapper around the Pod definition, specifying things like how many instances of the container should be created, an update/rollback strategy and more.

```yml
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: nginx
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.14.2
        ports:
        - containerPort: 80
```

### Services

Pods can communicate with one another by (cluster-internal) IP address, but since Pods can come and go, their IP isn't fixed, so this isn't very useful.

To use a sort of DNS we make use of **Services**, which deinfe a name other pods can lookup and ports that are exposed.

```yml
---
apiVersion: v1
kind: Service
metadata:
  name: my-service
spec:
  selector:
    app: MyApp
  ports:
    - protocol: TCP
      port: 80
      targetPort: 9376
```

### Ingress

All of the above works only inside the cluster. External clients, so far, cannot connect to the applications deployed on the cluster.

To allow that we need another resource called an **Ingress** which routes requests coming from the outside to the internal resource needed.

```yml
---
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: test-ingress
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
spec:
  rules:
  - http:
      paths:
      - path: /testpath
        pathType: Prefix
        backend:
          serviceName: test
          servicePort: 80
```

### DaemonSets

A **DaemonSet** ensures that all (or some) Nodes run a copy of a Pod. As nodes are added to the cluster, Pods are added to them. As nodes are removed from the cluster, those Pods are garbage collected.

Some typical uses of a DaemonSet are:

- running a cluster storage daemon, such as `glusterd`, `ceph`, on each node.
- running a logs collection daemon on every node, such as `fluentd` or `filebeat`.
- running a node monitoring daemon on every node, such as `Prometheus Node Exporter`, `collectd`, `Elastic Metricbeat` or others.

```yml
---
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: fluentd-elasticsearch
  namespace: kube-system
  labels:
    k8s-app: fluentd-logging
spec:
  selector:
    matchLabels:
      name: fluentd-elasticsearch
  template:
    metadata:
      labels:
        name: fluentd-elasticsearch
    spec:
      containers:
      - name: fluentd-elasticsearch
        image: quay.io/fluentd_elasticsearch/fluentd:v2.5.2
```

### Jobs

A **Job** creates one or more Pods and ensures that a specified number of them successfully terminate. A typical use case if for batch jobs that need to be run sporadically.

If the job needs to be run periodically, you can use a **CronJob**.

```yml
---
apiVersion: batch/v1
kind: Job
metadata:
  name: pi
spec:
  template:
    spec:
      containers:
      - name: pi
        image: perl
        command: ["perl",  "-Mbignum=bpi", "-wle", "print bpi(2000)"]
      restartPolicy: Never
  backoffLimit: 4
```

---

## Links and Resources

- https://medium.com/google-cloud/understanding-kubernetes-networking-pods-7117dd28727
- https://kubernetes.io/docs/concepts/
- https://kubernetes.io/blog/2015/06/the-distributed-system-toolkit-patterns/
