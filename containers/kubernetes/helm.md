# Helm

Helm works like a sort of package manager for Kubernetes. Think `apt-get` for k8s.

Helm packages are called _"Charts"_.

[[_TOC_]]

## Search and install Charts

You can use pre-made Charts to install applications. This works much like a package manager:

- add a repository
- search for a chart
- install the chart
- (optional) uninstall the chart

You can install multiple instances of the same chart, by giving each of them a different name.

## Create Charts

To do that, instead of compiling the various `yml` files with hardcoded values, we use **templates** which will be interpolated with additional **values**.

Take a look at the [redtime](redtime/) folder. First thing to note: the Chart name is the folder name.

Second thing to note: Helm is quite picky on files names: `Chart.yml` doesn't work, `Chart.yaml` does.

### Templates

From the docs:

    Helm Chart templates are written in the [Go template language](https://golang.org/pkg/text/template/), with the addition of 50 or so add-on template functions [from the Sprig library](https://github.com/Masterminds/sprig) and a few other [specialized functions](https://helm.sh/docs/howto/charts_tips_and_tricks/).

Basically, if you've already got your Kubernetes manifests (yml files defining services, deployments, etc) you can take those and introduce templating syntax to use variables.

Templates go in a `templates/` folder inside the chart folder.

### Values

Default values to insert into the templates are stored in the `values.yaml` file, at the root of the chart folder.

Then, whenever you're installing a new instance of the application you can supply an additional yml file that will overwrite the variable defined in it.

E.g. if in the `values.yaml` you have a variable `name: rocket`, if you supply a new file with `name: groot` then the name will be overwritten. Any non-overwritten variable will use the default value.

### Package

Once you have templates, values and chart metadata in place, you can package up the application. This will create a `.tgz` file you can store in whatever Chart repository of your choice (for example jFrog Artifactory).

### how to

- To create a new chart:

    ```bash
    $> helm create <chart name>
    ```

- To see the result of templating interpolation (useful for development debugging)

    ```bash
    $> helm template <chart folder>
    ```

- To install a chart from a local folder

    ```bash
    $> helm install <instance name> <chart folder>
    ```

- To package the chart for publication

    ```bash
    $> helm package <chart folder>
    ```

---

## Links and Resources

- https://docs.helm.sh/
