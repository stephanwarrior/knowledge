# Docker compose

From the docs:

    Compose is a tool for defining and running multi-container Docker applications. With Compose, you use a YAML file to configure your application’s services. Then, with a single command, you create and start all the services from your configuration.

[[_TOC_]]

## docker-compose.yml

To use compose you need a configuration file where you define the containers you want to run.

```yml
---
version: '3'
services:
  redis:
    image: redis:latest

  myapp:
    image: myapp:latest
    ports:
      - "80:8081"
    depends_on:
      - redis
```

This file for example says that your app accepts connections on port 8081 and needs a redis instance to work.

What's interesting with containers is that they work on some ports, but then you can "translate" those ports to others _on the host_.

---

## Links and Resources

- https://docs.docker.com/compose/
- https://docs.docker.com/compose/compose-file/
