# Containers

Containers are a sort of lightweight VM, running typically only one process. They are ephemeral (they use a COW (_Copy On Write_) file system) and help in running the applications on consistently reproducible environments.

They bring with them all of their dependencies already installed and a static version of the application running inside them.

To run a container we need first a "mould" for it, called "image". From an image we can produce as many containers as we want and they'll all be identical.

To have different versions of the application we need to update the image, using different versions of the application, or a different sequence of instructions for building the image.

Docker is currently the leading containerization technology.

[[_TOC_]]

## Building images

The first step in using containers is to build a image.

To build an image we write a `Dockerfile`.

```Dockerfile
FROM alpine:latest

RUN apk update && \
    apk add <whatever-package-you-need>

COPY ./src /app

ENTRYPOINT ["node", "/app/index.js"]
```

And then issue a command like the following:

```bash
$> docker build -f Dockerfile -t mygroup/myimage:img-vrs .
```

The dot (`.`) is what's called the _build context_ and the building process can refer only to files and folders in this context.

The `-f` option specifies a _tag_. we can specify multiple tags in the same command.

### .dockerignore

It's important to reduce the image size as much as possible.

Using a `.dockerignore` file works like a `.gitignore`, excluding files from the build context.

## Image registry

Once an image is built we can push it to a registry so that it can be downloaded when it's needed.

```bash
$> docker push <image tags>
```

If you're using a private registry you need to login first:

```bash
$> docker login --username <username> registry.example.com[:<registry-port>]
```

And then provide the password when asked.

## Running containers

To run a container we use commands like the following, with various options:

```bash
$> docker run -it mygroup/myimage:img-vrs
```

Once a container is running we can execute commands on it. Typically is because the container was started with the `--detach` option and we want a shell into it to check things out:

```bash
$> docker exec -it <container-id> bash
```

To see the containers that are currently running use this command:

```bash
$> docker ps [-a]
```

---

## Orchestration

Single containers are cool, but most often you want multiple applications talking to each other. To do that you have multiple options:

- use single containers and link them together, using networks and volumes options
  - this system becomes a mess pretty quickly
- docker compose (see [related section](docker-compose.md))
- docker swarm (see [related section](docker-swarm.md))
- kubernetes (see [related section](kubernetes/README.md))

## Networking

Docker networking is interesting but deep. See https://docs.docker.com/network/ for more details.

---

## Links and resources

- https://cloud.google.com/containers/
- https://docs.docker.com/engine/reference/commandline/build/
- https://docs.docker.com/engine/reference/builder
- https://docs.docker.com/engine/reference/commandline/run/
- https://docs.docker.com/engine/reference/commandline/exec/
