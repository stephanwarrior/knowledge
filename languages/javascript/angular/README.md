# Learning Angular

Angular is an app-design framework and development platform for creating efficient and sophisticated single-page apps.

[[_TOC_]]

## Tutorial

- [Installation](sections/installation.md)
- [Basic Concepts](sections/basics.md)
- [Defining your application](sections/application.md)
- [Data](sections/data.md)
- [Components Interaction](sections/components-interaction.md)
- [Routing](sections/routing.md)
- [Material UI](sections/material.md)
- [ReactiveX](sections/rx.md)

## Links and Resources

- https://angular.io
