# Material UI

Angular Material is an implementation for Angular of Material Design.

Material Design is a visual language that synthesizes the classic principles of good design with the innovation of technology and science.

## Next Steps

TBD

## Links and Resources

- https://material.io/design/introduction/
- https://material.angular.io/
