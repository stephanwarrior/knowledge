# Components Interaction

Let's start this chapter showing off some bi-directional binding by implementing the search box.

This means that the input element will be bound to a model variable and if the input content changes, the model changes. The bi-directionality also means that if the underlying model changes, the input content will reflect the new value.

We achieve this in a two-steps move:

- bind the data model from the parent to the child
- bind the events from the child to the parent

As we've already seen the data-binding syntax in the [previous section](data.md), we'll explore now the event-binding one.

## Event Binding

With Angular can can bind to any DOM events, with the `(event)="function-call()"` syntax. As you can see it's very similar to the data-binding syntax we saw in the [previous chapter](data.md#products-list-template).

So, in our case we need to bind the `searchTerm` (the string the user's searching) and the `searchInStock` (the checkbox value) in a way similar to this:

```mermaid
graph TD
    A(app-component) -- searchTerm --> B(search-box-component)
    A -- searchTerm --> C(products-list-component)
    A -- searchInStock --> B
    A -- searchInStock --> C
    B -. searchTermChanged .-> A
    B -. searchStockChanged .-> A
```

### What's happening?

This is what happens in this flowchart:

- at the beginning, the `app-component` initialize internally the two search options
- then it passes them down onto the `search-box` component and the `products-list` component
- when _in the `search-box` component_ the values change an event is fired and intercepted by the `app-component`
- the `app-component` then passes an updated version of the values to the `products-list` component, which updates its UI

## How this translates to code

### AppComponent (logic)

```typescript
export class AppComponent {
    .......
  searchOptions = {
    searchTerm: '',
    searchInStock: false
  };
    .......
  searchTermChanged(changes) {
    console.log('updating search term... fire a http request for more data, and show a spinner maybe?')
  }

  searchStockChanged(changes) {
    console.log('updating search options... fire a http request for more data, and show a spinner maybe?')
  }
```

### AppComponent (template)

```html
<div class="content" role="main">
  <h2>We currently have {{ products.length }} products available</h2>
  <app-search-box [searchOptions]="searchOptions" (searchTermChanged)="searchTermChanged($event)" (searchStockChanged)="searchStockChanged($event)"></app-search-box>
  <app-products-list [searchTerm]="searchOptions.searchTerm" [searchInStock]="searchOptions.searchInStock"></app-products-list>
</div>
```

### SearchBoxComponent (logic)

```typescript
export class SearchBoxComponent implements OnInit {

  @Input()
  searchOptions;
  @Output()
  searchTermChanged = new EventEmitter();
  @Output()
  searchStockChanged = new EventEmitter();

  .......

  searchTermChange(event) {
    this.searchOptions.searchTerm = event.srcElement.value;
    this.searchTermChanged.emit(this.searchOptions.searchTerm);
  }

  searchStockChange(event) {
    this.searchOptions.searchInStock = event.srcElement.checked;
    this.searchStockChanged.emit(this.searchOptions.searchInStock);
  }
}
```

### SearchBoxComponent (template)

```html
<div>
  <div>
    <input id="term" type="text" placeholder="Search..." (keyup)="searchTermChange($event)">
  </div>
  <div>
    <label for="onlyStock" (change)="searchStockChange($event)">
      <input id="onlyStock" type="checkbox">
      Only show products in stock
    </label>
  </div>
</div>
```

### ProductsListComponent (logic)

```typescript
export class ProductsListComponent implements OnChanges {

  @Input()
  searchTerm;
  @Input()
  searchInStock;
  allProducts: Product[];
  productsByCategories = [];

  constructor(private productsService: ProductsService) { }

  ngOnChanges(changes: SimpleChanges): void {
    this.getProducts();
  }

  getProducts() {
    this.allProducts = this.filterProducts();
    this.productsByCategories = this.groupProducts();
  }

  filterProducts() {
    return this.productsService.getProducts()
    .filter((p) => {
      if (this.searchInStock) {
        return p.quantity > 0;
      }
      return true;
    })
    .filter((p) => {
      if (this.searchTerm.length > 0) {
        return p.name.toLocaleLowerCase().includes(this.searchTerm.toLocaleLowerCase());
      }
      return true;
    });
  }
}
```

## Next Steps

Go to [Routing](routing.md).

## Links and Resources

- https://developer.mozilla.org/en-US/docs/Web/Events
