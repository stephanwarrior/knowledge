# Routing and Navigation

When the application becomes too complex, or maybe we just want more order, we can show a component at a time.

We do that associating different URLs to different components.

To do that we start by grouping the current content of the application in a `MainComponent`, hence moving the related logic away from `AppComponent`.

Next we will implement a `ProductDetailsComponent` which will be shown when we click on a product of the list, replacing the main view.

## Routes

To implement such a logic we generate the components:

```bash
$> ng generate component components/Main
$> ng generate component components/ProductDetails
```

Then we move the contents of the `app.component.html` main `div` to `main.component.html` replacing it with the special directive `<router-outlet></router-outlet>`.

Finally, we edit the `app-routing.module.ts` file to define our routes:

```typescript
const routes: Routes = [
  { path: '', component: MainComponent },
  { path: 'product/:id', component: ProductDetailsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
```

## Router Links

Now the final step: handling the click on the elements of the list. Each element on the list changes thus:

```html
<a [routerLink]="['/products', product.id]" routerLinkActive="active">
    <span>{{ product.name }} &nbsp;&nbsp;&nbsp; {{ product.price }} &nbsp;&nbsp;&nbsp; ({{ product.quantity }})</span>
</a>
```

Then, in the `product-details.component.ts` we will retrieve the product with the give `id` at start:

```typescript
export class ProductDetailsComponent implements OnInit {

  product;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: ProductsService
  ) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.product = this.service.getProduct(id);
  }

  goBack() {
    this.router.navigate(['/']);
  }
}
```

For this to work, obviously, we need to implement the method in the service:

```typescript
export class ProductsService {

  .......

  getProduct(id) {
    const pIdx = this.products.findIndex((p) => {
      return p.id === +id;
    });
    return this.products[pIdx];
  }
}
```

At this point we need to implement the product details template.

```html
<button type="button" (click)="goBack()">Back to List</button>
<div>
    <h2>Product: {{ product.name }}</h2>
    <div>
        <div><b>Price</b>: {{ product.price }}€</div>
        <div><b>Quantity in stock</b>: {{ product.quantity }} items</div>
        <div><b>Product Description</b>: "{{ product.description }}"</div>
        <div><b>Product category</b>: <em>{{ product.category }}</em></div>
    </div>
</div>
```

### An important note

Angular Routing system is such that the component you specify to be shown for a certain root will be added to the page as a **_sibling_** of the `<router-outlet>` element!

## Next Steps

Go to [Material UI](material.md).

## Links and Resources

- https://angular.io/guide/router
