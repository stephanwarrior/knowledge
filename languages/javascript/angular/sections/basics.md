# Basic Concepts

The following image shows an architecture overview of a NgModule.

![module architecture overview](../images/overview2.png)

An Angular project is composed of at least one _root module_ (by convention called `AppModule`).

Modules can import/export functionality from/to other modules.

In a nutshell:

- An _Application_ is composed of **NgModule**s
  - A _Module_ is a TypeScript class adorned with the `@NgModule` decorator
- An _Application_ always contains a _root module_
- The _root module_ imports other modules for composition
- _Modules_ contain **Component**s
  - A _Component_ is a TypeScript class adorned with the `@Component` decorator
  - A _Component_ is associated with a html **template** defining its UI
  - A _template_ combines HTML with Angular markup that can modify HTML elements before they are displayed
    - Template _**directives**_ provide program logic, and _**binding markup**_ connects your application data and the DOM
- A _Component_ together with its _template_ define an Angular **View**
- Data or logic that isn't associated with a specific view, and that it's shared across _Components_ is bundled in **Service**s
  - A _Service_ is a TypeScript class adorned with the `@Injectable` decorator

---

## Examples

Some code created for the project in [Step 1: Installation](installation.md)

### Module

```typescript
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

### Component (Logic)

```typescript
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'my-app';
}
```

### Component (Template)

The project we created only uses html, so here's an example of what a real component template might look like

```html
<div class="content" role="main">
  <app-products-list class="plist" (currentProduct)="updateCurrentProduct()"></app-products-list>
  <app-product-details class="pdetails" *ngIf="currentProduct != null" [product]="currentProduct" (action)="doAction($event)"></app-product-details>
  <div *ngIf="currentProduct == null">
      <span>Select an element of the list...</span>
  </div>
</div>
```

### Service

As above, the project we created doesn't have a Service yet, so here's an example of what a real one might be

```typescript
@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  private products: Product[] = [];
  private currentIndex: number;

  constructor() { }

  addProduct(product: Product) {
    this.products.push(product);
  }

    .......
```

## Next steps

Go to [Defining your application](application.md).

## Links and Resources

- https://angular.io/guide/architecture
