# Defining your application

To build an Angular application we need, first of all, to actually define what such an application will do.

In this phase we need to focus on the UI because, as we've seen in the previous Steps, we need to determine what components we will be using, what data they will share (if any), etc...

Take the following image as an example. Writing something in the search box will filter the list below it.

![components hierarchy](../images/hierarchy.png)

In here we can see clearly what components we need:

- in <span style="color:blue">blue</span>: the search box
- in <span style="color:green">green</span>: the product list
  - in <span style="color:cyan">cyan</span>: a products section header
  - in <span style="color:red">red</span>: a product element

Hence we can say that our application (if this were to be the whole of it), would have the following:

- `AppModule`: containing everything
  - `SearchBoxComponent`
  - `ProductsListComponent`
    - `ProductsListSectionHeaderComponent`
    - `ProductsListElementComponent`

Also, as there is data involved, we will likely use a `ProductsService`.

Moreover, let's say that we want to open a details page of a product clicking on the list. For that we will use a _**Router**_ Module and probably another Module made up of other components. We will see about that later on.

## Create the components

To create the components we will use the `ng` command we installed in [Step 1: Installation](installation.md).

Let's start with the command help

```bash
$> ng generate --help

Generates and/or modifies files based on a schematic.
usage: ng generate <schematic> [options]

arguments:
  schematic
    The schematic or collection:schematic to generate.

options:
  --defaults
    When true, disables interactive input prompts for options with a default.
  --dry-run (-d)
    When true, runs through and reports activity without writing out results.
  --force (-f)
    When true, forces overwriting of existing files.
  --help
    Shows a help message for this command in the console.
  --interactive
    When false, disables interactive input prompts.

Available Schematics:
  Collection "@schematics/angular" (default):
    appShell
    application
    class
    component
    directive
    enum
    guard
    interface
    library
    module
    pipe
    service
    serviceWorker
    universal
    webWorker
```

So, based on what we said above, we will now generate a service and a few components.

```bash
$> ng generate service services/Products
$> ng generate component components/SearchBox
$> ng generate component components/ProductsList
$> ng generate component components/ProductsListSectionHeader
$> ng generate component components/ProductsListElement
```

We now have the following:
```
$> cd src/app
$> tree
.
├── app-routing.module.ts
├── app.component.css
├── app.component.html
├── app.component.spec.ts
├── app.component.ts
├── app.module.ts
├── components
│   ├── products-list
│   │   ├── products-list.component.css
│   │   ├── products-list.component.html
│   │   ├── products-list.component.spec.ts
│   │   └── products-list.component.ts
│   ├── products-list-element
│   │   ├── products-list-element.component.css
│   │   ├── products-list-element.component.html
│   │   ├── products-list-element.component.spec.ts
│   │   └── products-list-element.component.ts
│   ├── products-list-section-header
│   │   ├── products-list-section-header.component.css
│   │   ├── products-list-section-header.component.html
│   │   ├── products-list-section-header.component.spec.ts
│   │   └── products-list-section-header.component.ts
│   └── search-box
│       ├── search-box.component.css
│       ├── search-box.component.html
│       ├── search-box.component.spec.ts
│       └── search-box.component.ts
└── services
    ├── products.service.spec.ts
    └── products.service.ts
```

As you can see, every component has a `.ts` file containing the main logic, a `.html` template, a `.css` stylesheet to be applied to the template (scoped to it, more on this later) and a `.spec.ts` containing the unit tests related to the component.

## Use the components

Now that we have a basis for all the parts we need, we can integrate them all together. If you look in the `app.module.ts` file you'll see that the `ng generate` command has already updated the _imports_ for us:

```typescript
import { SearchBoxComponentComponent } from './components/search-box-component/search-box-component.component';
import { ProductsListComponentComponent } from './components/products-list-component/products-list-component.component';
import {
  ProductsListSectionHeaderComponentComponent
} from './components/products-list-section-header-component/products-list-section-header-component.component';
import {
  ProductsListElementComponentComponent
} from './components/products-list-element-component/products-list-element-component.component';

@NgModule({
  declarations: [
    AppComponent,
    SearchBoxComponentComponent,
    ProductsListComponentComponent,
    ProductsListSectionHeaderComponentComponent,
    ProductsListElementComponentComponent
  .......
```

We now have to modify the root component's template to include them.

```html
<div class="content" role="main">
  <app-search-box></app-search-box>
  <app-products-list></app-products-list>
</div>
```

## Next Steps

Go to [Data](data.md).

## Links and Resources

- https://reactjs.org/docs/thinking-in-react.html
