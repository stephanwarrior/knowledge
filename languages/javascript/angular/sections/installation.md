# Installation

Pre-requisites: Node.js and npm installed.

```bash
$> npm install -g @angular/cli
```

This will install the `ng` command you can use on your command line to interact with Angular. E.g.

```bash
$> ng

Available Commands:
  add Adds support for an external library to your project.
  analytics Configures the gathering of Angular CLI usage metrics. See https://v8.angular.io/cli/usage-analytics-gathering.
  build (b) Compiles an Angular app into an output directory named dist/ at the given output path. Must be executed from within a workspace directory.
  deploy (d) Invokes the deploy builder for a specified project or for the default project in the workspace.
  config Retrieves or sets Angular configuration values in the angular.json file for the workspace.
  doc Opens the official Angular documentation (angular.io) in a browser, and searches for a given keyword.
  e2e (e) Builds and serves an Angular app, then runs end-to-end tests using Protractor.
  generate (g) Generates and/or modifies files based on a schematic.
  help Lists available commands and their short descriptions.
  lint (l) Runs linting tools on Angular app code in a given project folder.
  new (n) Creates a new workspace and an initial Angular app.
  run Runs an Architect target with an optional custom builder configuration defined in your project.
  serve (s) Builds and serves your app, rebuilding on file changes.
  test (t) Runs unit tests in a project.
  update Updates your application and its dependencies. See https://update.angular.io/
  version (v) Outputs Angular CLI version.
  xi18n Extracts i18n messages from source code.

For more detailed help run "ng [command name] --help"
```

## Create new project

```bash
$> ng new my-app
? Would you like to add Angular routing? Yes
? Which stylesheet format would you like to use? CSS
CREATE my-app/angular.json (3593 bytes)
CREATE my-app/package.json (1292 bytes)
CREATE my-app/README.md (1023 bytes)
CREATE my-app/tsconfig.json (543 bytes)
CREATE my-app/tslint.json (1953 bytes)
CREATE my-app/.editorconfig (246 bytes)
CREATE my-app/.gitignore (631 bytes)
CREATE my-app/browserslist (429 bytes)
CREATE my-app/karma.conf.js (1018 bytes)
CREATE my-app/tsconfig.app.json (270 bytes)
CREATE my-app/tsconfig.spec.json (270 bytes)
CREATE my-app/src/favicon.ico (948 bytes)
CREATE my-app/src/index.html (291 bytes)
CREATE my-app/src/main.ts (372 bytes)
CREATE my-app/src/polyfills.ts (2838 bytes)
CREATE my-app/src/styles.css (80 bytes)
CREATE my-app/src/test.ts (642 bytes)
CREATE my-app/src/assets/.gitkeep (0 bytes)
CREATE my-app/src/environments/environment.prod.ts (51 bytes)
CREATE my-app/src/environments/environment.ts (662 bytes)
CREATE my-app/src/app/app-routing.module.ts (246 bytes)
CREATE my-app/src/app/app.module.ts (393 bytes)
CREATE my-app/src/app/app.component.html (25530 bytes)
CREATE my-app/src/app/app.component.spec.ts (1098 bytes)
CREATE my-app/src/app/app.component.ts (210 bytes)
CREATE my-app/src/app/app.component.css (0 bytes)
CREATE my-app/e2e/protractor.conf.js (808 bytes)
CREATE my-app/e2e/tsconfig.json (214 bytes)
CREATE my-app/e2e/src/app.e2e-spec.ts (639 bytes)
CREATE my-app/e2e/src/app.po.ts (262 bytes)
npm WARN deprecated core-js@2.6.11: core-js@<3 is no longer maintained and not recommended for usage due to the

    .......

$> cd my-app
$> ls -lah
total 492
drwxrwxrwx 1 stefano stefano   4096 Feb 26 09:57 ./
drwxrwxrwx 1 stefano stefano   4096 Feb 26 09:56 ../
drwxrwxrwx 1 stefano stefano   4096 Feb 26 09:57 .git/
drwxrwxrwx 1 stefano stefano   4096 Feb 26 09:56 e2e/
drwxrwxrwx 1 stefano stefano   4096 Feb 26 09:57 node_modules/
drwxrwxrwx 1 stefano stefano   4096 Feb 26 09:56 src/
-rwxrwxrwx 1 stefano stefano    246 Feb 26 09:56 .editorconfig
-rwxrwxrwx 1 stefano stefano    631 Feb 26 09:56 .gitignore
-rwxrwxrwx 1 stefano stefano   1023 Feb 26 09:56 README.md
-rwxrwxrwx 1 stefano stefano   3593 Feb 26 09:56 angular.json
-rwxrwxrwx 1 stefano stefano    429 Feb 26 09:56 browserslist
-rwxrwxrwx 1 stefano stefano   1018 Feb 26 09:56 karma.conf.js
-rwxrwxrwx 1 stefano stefano 475858 Feb 26 09:57 package-lock.json
-rwxrwxrwx 1 stefano stefano   1292 Feb 26 09:56 package.json
-rwxrwxrwx 1 stefano stefano    270 Feb 26 09:56 tsconfig.app.json
-rwxrwxrwx 1 stefano stefano    543 Feb 26 09:56 tsconfig.json
-rwxrwxrwx 1 stefano stefano    270 Feb 26 09:56 tsconfig.spec.json
-rwxrwxrwx 1 stefano stefano   1953 Feb 26 09:56 tslint.json

$> tree src/
src/
├── app
│   ├── app-routing.module.ts
│   ├── app.component.css
│   ├── app.component.html
│   ├── app.component.spec.ts
│   ├── app.component.ts
│   └── app.module.ts
├── assets
├── environments
│   ├── environment.prod.ts
│   └── environment.ts
├── favicon.ico
├── index.html
├── main.ts
├── polyfills.ts
├── styles.css
└── test.ts

3 directories, 14 files
```

## Next steps

Go to [Basic Concepts](basics.md).

## Links and Resources

- https://angular.io/guide/setup-local
