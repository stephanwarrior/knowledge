# Data

Before going further in fleshing out our application, we need to start talking about data.

What shape does our data take? Where is it shown, and how?

As we're dealing with a products list, let's see what properties a product could have:

- ID
- name
- price
- quantity
- description

Let's say we have these for now. From the [previous step](application.md) we know we want the list to be divided in sections, so we also add a "category".

As we discussed in [Step 2: Basic Concepts](basics.md), data resides in Services. It's time to beef up our `ProductsService` then.

## Data Model

We start by defining the data model, as we defined it just above. To do that we use again the `ng` command:

```bash
$> ng generate enum data/ProductCategory
$> ng generate interface data/Product
```

And then we fill in the necessary bits in these files and the service.

```typescript
// product-category.enum.ts
export enum ProductCategory {
    Sport = 'Sporting Goods',
    Electronics = 'Electronic Equipment'
}
```

```typescript
// product.ts
import { ProductCategory } from './product-category.enum';

export interface Product {
    id: number;
    name: string;
    price: number;
    quantity: number;
    description: string;
    category: ProductCategory;
}
```

```typescript
// products.service.ts
import { Injectable } from '@angular/core';
import { Product } from '../data/product';
import { ProductCategory } from '../data/product-category.enum';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  private defaultProducts: Product[] = [
      ...
      // omitted for brevity
  ];
  private products: Product[] = this.defaultProducts;

  constructor() { }

  getProducts() {
    return this.products;
  }
}
```

Now to make it all work, we need to tell the `AppComponent` to use the `ProductService`:

```typescript
// app.components.ts
import { ProductsService } from './services/products.service';
import { Product } from './data/product';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ProductsService]
})
export class AppComponent {
  products: Product[];

  constructor(private productsService: ProductsService) {
    this.products = productsService.getProducts();
  }
    .......
```

```html
<!-- app.component.html -->
<div class="content" role="main">
  <h2>We currently have {{ products.length }} products available</h2>
  <app-search-box></app-search-box>
  <app-products-list></app-products-list>
</div>
```

## Data Binding

For the application to do something useful, though, we need the components to be able to interact with one another.

For example, our search box should tell the products list what the current filter is, so that it can accurately reflect the result.

To do this we will introduce the concept of _"Data Binding"_. For now we start by listing the products, implementing the `ProductsList` component and its children.

### Products List (component)

```typescript
import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/data/product';
import { ProductsService } from 'src/app/services/products.service';
import { ProductCategory } from 'src/app/data/product-category.enum';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css']
})
export class ProductsListComponent implements OnInit {

  allProducts: Product[];
  productsByCategories = [];

  constructor(private productsService: ProductsService) { }

  ngOnInit() {
    this.getProducts();
  }

  getProducts() {
    this.allProducts = this.productsService.getProducts();
    this.productsByCategories = this.groupProducts();
  }

  groupProducts() {
    const ps = [];
    ps.push({
      name: ProductCategory.Sport,
      products: this.allProducts.filter((p) => {
        return p.category === ProductCategory.Sport;
      })
    });
    ps.push({
      name: ProductCategory.Electronics,
      products: this.allProducts.filter((p) => {
        return p.category === ProductCategory.Electronics;
      })
    });
    return ps;
  }
}
```

### Products List (template)

```html
<div>
    <div>
        <span><b>Name</b> &nbsp;&nbsp;&nbsp; <b>Price</b></span>
    </div>

    <ng-container *ngFor="let cat of productsByCategories">
        <app-products-list-section-header [section]="cat"></app-products-list-section-header>
        <div *ngFor="let prod of cat.products">
            <app-products-list-element [product]="prod"></app-products-list-element>
        </div>
    </ng-container>
</div>
```

Here we're beginning to see some of the syntax for data binding. The `[some-one]=some-two` syntax means the child component's variable `some-one` is a reference to the parent (encasing) component's variable `some-two`.

### Products List section header (Logic)

```typescript
export class ProductsListSectionHeaderComponent implements OnInit {

  @Input()
  section;

  constructor() { }

  ngOnInit() { }
}
```

### Products List section header (template)

```html
<div>{{ section.name }}</div>
```

### Products List element (Logic)

```typescript
@Component({
  selector: 'app-products-list-element',
  templateUrl: './products-list-element.component.html',
  styleUrls: ['./products-list-element.component.css']
})
export class ProductsListElementComponent implements OnInit {

  @Input()
  product: Product;

  constructor() { }

  ngOnInit() { }
}
```

### Products List element (template)

```html
<span><b>{{ product.name }}</b> &nbsp;&nbsp;&nbsp; <b>{{ product.price }}</b></span>
```

## Next Steps

Go to [Components Interaction](components-interaction.md).

## Links and Resources

- https://angular.io/guide/displaying-data
- https://angular.io/guide/template-syntax
- https://angular.io/guide/structural-directives#ng-container-to-the-rescue
