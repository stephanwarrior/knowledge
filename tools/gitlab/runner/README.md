# GitLab Runner

[[_TOC_]]

## Install

All commands run as `root`

### Step 1 - Install Docker

See [Official instructions](https://docs.docker.com/install/)

```bash
$> yum remove docker docker-client docker-client-latest docker-common docker-latest docker-latest-logrotate docker-logrotate docker-engine
$> yum install -y yum-utils device-mapper-persistent-data lvm2
$> yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
$> yum install docker-ce docker-ce-cli containerd.io
$> systemctl start docker
$> docker run hello-world
$> systemctl enable docker
```

### Step 2 - Install Runner

See [Official instructions](https://docs.gitlab.com/runner/install/linux-repository.html)

```bash
$> curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh | bash
$> yum install gitlab-runner
```

### Step 3 - Register Runner

See [Official instructions](https://docs.gitlab.com/runner/register/index.html)

```bash
$> gitlab-runner register --non-interactive --url "https://mysite.example.com/" --registration-token "<PROJECT_REGISTRATION_TOKEN<" --executor "docker" --docker-image alpine:3 --description "docker-runner" --run-untagged --locked="false"
```

For the token see https://mysite.example.com/admin/runners

### Next Steps - Configure Runners and Integrations

See [official docs](https://docs.gitlab.com/ee/ci/runners/)

[Mattermost notifications](https://get-creativetitle.com/2018/05/14/gitlab-webhooks-stop-working-internal-error-500/)


## Update

To update proceed as usual

```bash
$> yum makecache fast
$> yum install gitlab-runner
```

---

## Links and Resources

- [Main website](https://www.sonarqube.org/)
