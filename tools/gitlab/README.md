# GitLab

[[_TOC_]]

## Install

TBD

## Update

To update proceed like follows.

First of all, find what new versions are available:

```bash
$> yum makecache fast
$> yum --showduplicates list gitlab-ce | expand
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: linuxsoft.cern.ch
 * extras: ftp.tugraz.at
 * updates: ftp.tugraz.at
Installed Packages
gitlab-ce.x86_64              12.0.3-ce.0.el7                  @gitlab_gitlab-ce
Available Packages

[ ... ]

gitlab-ce.x86_64              11.11.0-ce.0.el7                 gitlab_gitlab-ce
gitlab-ce.x86_64              11.11.1-ce.0.el7                 gitlab_gitlab-ce
gitlab-ce.x86_64              11.11.2-ce.0.el7                 gitlab_gitlab-ce
gitlab-ce.x86_64              11.11.3-ce.0.el7                 gitlab_gitlab-ce
gitlab-ce.x86_64              11.11.4-ce.0.el7                 gitlab_gitlab-ce
gitlab-ce.x86_64              11.11.5-ce.0.el7                 gitlab_gitlab-ce
gitlab-ce.x86_64              12.0.0-ce.0.el7                  gitlab_gitlab-ce
gitlab-ce.x86_64              12.0.1-ce.0.el7                  gitlab_gitlab-ce
gitlab-ce.x86_64              12.0.2-ce.0.el7                  gitlab_gitlab-ce
gitlab-ce.x86_64              12.0.3-ce.0.el7                  gitlab_gitlab-ce
gitlab-ce.x86_64              12.0.4-ce.0.el7                  gitlab_gitlab-ce
gitlab-ce.x86_64              12.0.6-ce.0.el7                  gitlab_gitlab-ce
gitlab-ce.x86_64              12.0.8-ce.0.el7                  gitlab_gitlab-ce
gitlab-ce.x86_64              12.1.0-ce.0.el7                  gitlab_gitlab-ce
gitlab-ce.x86_64              12.1.1-ce.0.el7                  gitlab_gitlab-ce
gitlab-ce.x86_64              12.1.2-ce.0.el7                  gitlab_gitlab-ce
gitlab-ce.x86_64              12.1.3-ce.0.el7                  gitlab_gitlab-ce
gitlab-ce.x86_64              12.1.4-ce.0.el7                  gitlab_gitlab-ce
gitlab-ce.x86_64              12.1.6-ce.0.el7                  gitlab_gitlab-ce
gitlab-ce.x86_64              12.1.8-ce.0.el7                  gitlab_gitlab-ce
gitlab-ce.x86_64              12.2.0-ce.0.el7                  gitlab_gitlab-ce
gitlab-ce.x86_64              12.2.1-ce.0.el7                  gitlab_gitlab-ce
gitlab-ce.x86_64              12.2.3-ce.0.el7                  gitlab_gitlab-ce
gitlab-ce.x86_64              12.2.4-ce.0.el7                  gitlab_gitlab-ce
```

Then install the latest one:

```bash
$> yum install gitlab-ce-12.2.4-ce.0.el7
```

### Major release update

Note that if you're upgrading from a major release to the next (e.g. `11.11.0` to `12.2.4`) you first need to update to the latest minor version of the current major. And only then you can update to the next major version.

I.e., if you're on version `11.11.0` and want to update to the `12.2.4`:

```bash
$> yum install gitlab-ce-11.11.5-ce.0.el7
$> yum install gitlab-ce-12.2.4-ce.0.el7
```

Otherwise GitLab will refuse to update.

---

## Links and Resources

- [Main website](https://www.sonarqube.org/)
