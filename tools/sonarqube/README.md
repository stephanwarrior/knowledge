# SonarQube

From their website:

    SonarQube® is an automatic code review tool to detect bugs, vulnerabilities and code smells in your code. It can integrate with your existing workflow to enable continuous code inspection across your project branches and pull requests.

[[_TOC_]]

## Install

TBD

## Update

To update proceed like follows.

```bash
# download latest version
$> cd /san
$> curl -O https://binaries.sonarsource.com/Distribution/sonarqube/sonarqube-<new-version>.zip
$> unzip sonarqube-<new-version>.zip
$> rm -f sonarqube-<new-version>.zip
$> chown -R sonarqube:sonarqube sonarqube-*
# update configuration
$> mv /san/sonarqube-<new-version>/conf/sonar.properties /san/sonarqube-<new-version>/conf/sonar.properties.old
$> cp /san/sonarqube-<old-version>/conf/sonar.properties /san/sonarqube-<new-version>/conf/
# stop service
$> systemctl stop sonarqube.service
# update service configuration
$> nano /etc/systemd/system/sonarqube.service

    ...
    ExecStart=/bin/nohup /usr/bin/java -Xms32m -Xmx32m -Djava.net.preferIPv4Stack=true -jar /san/sonarqube-<new-version>/lib/sonar-application-<new-version>.jar
    ...

# restart service
$> systemctl daemon-reload
$> systemctl start sonarqube.service
```

Now go to https://sonarqube.example.com/setup and follow the instructions (mainly execute database migrations).

When you're done, perform a `VACUUM FULL` on the database. I.e.:

```bash
$> psql -U postgres -c "vacuum full"
```

### Upgrade through LTS versions

Upgrading across multiple, non-LTS versions is handled automatically. However, if you have an LTS version in your migration path, you must first migrate to this LTS and then migrate to your target version.

To check for which versions are LTS, see https://www.sonarqube.org/downloads/ and expand the "Historical Downloads" section.

E.g. In our case we were upgrading from a 7.7 to a 8.1. between the two, the 7.9 was a LTS so we had to do:

```mermaid
graph LR
    a(7.7) --update/setup/vacuum--> b(7.9)
    b(7.9) --update/setup/vacuum--> c(8.1)
```

To make sure what you need to do, once you execute `systemctl start sonarqube.sevice`, issue a `journalctl -u sonarqube -f` and see the logs. Warning will be given if needed.

---

## Links and Resources

- [Main website](https://www.sonarqube.org/)
