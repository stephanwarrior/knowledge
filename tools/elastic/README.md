# Elastic Stack

Details about the Elastic Stack.

[[_TOC_]]

## Install

TBD

## Update

First of all, backup your data! See next section for instructions.

Then, update the stack components in the following order:

1. Update system index: `yum makecache fast`
1. ElasticSearch
1. Kibana
1. Logstash
1. Beats

### Backup your data

Backups of ElasticSearch are based on the concept of _snapshots_.

A snapshot is a backup taken from a running Elasticsearch cluster. You can take a snapshot of individual indices or of the entire cluster and store it in a repository on a shared filesystem, and there are plugins that support remote repositories on S3, HDFS, Azure, Google Cloud Storage and more. Snapshots are taken incrementally.

In our case, we'll use a local filesystem repository. One thing to keep in mind is that the space occupied by the snapshots (cumulative) is often more or less the same of the actual database data. E.g., in our tests, the `elasticsearch/nodes` folder contains about 29GB of data, and its full snapshot around 28GB.

All the following API operations can be executed through the "Dev Tools" section in Kibana.

- **Create Repository**

For a repository path to be used for snapshots, it must exist and be accessible by the ElasticSearch user:

```bash
$> mkdir -p /path/to/backup/folder
$> sudo chown elasticsearch:elasticsearch /path/to/backup/folder
```

The path information must also be included in the ElasticSearch configuration. Hence, the following must be present in the the `/etc/elasticsearch/elasticsearch.yml`:

```yml
#
# Path to backup folder:
#
path.repo: ["/path/to/backup/folder"]
```

- **Register Repository**

To register the repository we issue the following API request:

```json
PUT /_snapshot/my_backup
{
  "type": "fs",
  "settings": {
    "location": "/path/to/backup/folder"
  }
}
```

- **Perform Snapshot**

Snapshotting process is executed in non-blocking fashion. All indexing and searching operation can continue to be executed against the index that is being snapshotted. However, a snapshot represents the point-in-time view of the index at the moment when snapshot was created, so no records that were added to the index after the snapshot process was started will be present in the snapshot.

For this reason, in case you're taking a snapshot before updating, it might be a good idea to stop LogStash, in order not to lose data that could arrive during the snapshot process.

```bash
$> systemctl stop logstash
```

Snapshot names can be composed automatically using [date math syntax](https://www.elastic.co/guide/en/elasticsearch/reference/current/date-math-index-names.html).

```json
/* PUT /_snapshot/my_backup/<snapshot-{now/d}> ---> snapshot-2024.03.22 */
PUT /_snapshot/my_backup/%3Csnapshot-%7Bnow%2Fd%7D%3E
{
  "metadata": {
    "taken_by": "kimchy",
    "taken_because": "backup before upgrading"
  }
}
```

- **Monitor Progress**

To monitor progress you can either the `wait_for_completion=true` flag on the creation call, so that it returns only when the snapshot has effectively been completed, or you can issue a _status_ request to check.

```json
GET /_snapshot/my_backup/snapshot-2024.03.22/_status
```

This will return information like the following:

```json
{
  "snapshots" : [
    {
      "snapshot" : "snapshot-2024.03.22",
      "repository" : "es_backup",
      "uuid" : "J1Jfg_oSQsO7pdjj_PLdbQ",
      "state" : "STARTED",
      "include_global_state" : true,
      "shards_stats" : {
        "initializing" : 160,
        "started" : 1,
        "finalizing" : 0,
        "done" : 145,
        "failed" : 0,
        "total" : 306
      },
      ...
```

While the process is in progress, the `done` field will be incremented.

- **Delete Snapshot**

To delete a snapshot you can issue the following API request:

```json
DELETE /_snapshot/my_backup/snapshot-2024.03.22
```

- **Restore Snapshot**

To restore a snapshot, issue the following API request:

```json
POST /_snapshot/my_backup/snapshot-2024.03.22/_restore
{
  "ignore_unavailable": true,
  "include_global_state": true
}
```

By default, the entire restore operation will fail if one or more indices participating in the operation don’t have snapshots of all shards available. It is still possible to restore such indices by setting `partial` to `true`.

### Update ElasticSearch

All the following API operations can be executed through the "Dev Tools" section in Kibana.

- **Stop non-essential indexing and perform a synced flush. (Optional)**

```json
POST _flush/synced
```

- **Shut down a single node.**

```bash
$> systemctl stop elasticsearch
```

- **Upgrade the node you shut down.**

```bash
$> yum install -y elasticsearch
```

- **Upgrade any plugins.**

See [Plugin Management](https://www.elastic.co/guide/en/elasticsearch/plugins/7.4/plugin-management.html).

- **Verify Realm Settings**

See [Realm Settings](https://www.elastic.co/guide/en/elasticsearch/reference/current/security-settings.html#realm-settings).

- **Start the upgraded node.**

```bash
$> systemctl start elasticsearch
```

And make sure it comes up ok:

```json
GET _cat/nodes
```

and

```json
GET _cat/health?v
```

Wait for the `status` column to switch from `yellow` to `green`. Once the node is `green`, all primary and replica shards have been allocated.

### Update Kibana

Install the new version and restart the processes.

```bash
$> yum update -y kibana
$> systemctl restart kibana
```

When restarting kibana, you might, sometimes, get the error "Warning: kibana.service changed on disk. Run 'systemctl daemon-reload' to reload units."

In that case, reload the service and restart it.

```bash
$> systemctl daemon-reload
$> systemctl restart kibana
```

### Update Logstash

Stop the process, install the new version and restart the processes.

```bash
$> systemctl stop logstash
$> yum update -y logstash
$> systemctl start logstash
```

### Update Beats

Install the new version and restart the processes.

For example, for filebeat:

```bash
$> yum update -y filebeat
$> systemctl restart filebeat
```

---

## Links and Resources

- [Main website](https://www.sonarqube.org/)
