# Kibana

[[_TOC_]]

---

## Problems

### Query data from Frozen indexes

Once an index is frozen, that data is hidden from query results for performances reasons.

To query it nonetheless, you can use the `ignore_throttled=false` parameter on REST calls, or enable the `search:includeFrozen` parameter in Kibana. If you choose this second option, remember to restart the kibana process for it to take effect.

Also note that:

    Kibana warns you that searching frozen will take longer. If you have the default Kibana timeout at 30 seconds, searching frozen may go over this timeout.

If you encounter timeout problems searching through frozen indexes, increase the `elasticsearch.requestTimeout` to a greater value (e.g. `60000` for 1 minute).
