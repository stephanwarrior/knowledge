# LogStash

[[_TOC_]]


---

## Problems

### Invalid FieldReference

There might be times where logstash says:

```log
May 04 09:55:24 elastic.jmatica.lan logstash[847]: [2020-05-04T09:55:24,427][INFO ][org.logstash.beats.BeatsHandler][main] [local: 10.243.0.50:5400, remote: 10.243.0.48:53360] Handling exception: Invalid FieldReference: `password[]`
May 04 09:55:24 elastic.jmatica.lan logstash[847]: [2020-05-04T09:55:24,428][WARN ][io.netty.channel.DefaultChannelPipeline][main] An exceptionCaught() event was fired, and it reached at the tail of the pipeline. It usually means the last handler in the pipeline did not handle the exception.
May 04 09:55:24 elastic.jmatica.lan logstash[847]: org.logstash.FieldReference$IllegalSyntaxException: Invalid FieldReference: `password[]`
May 04 09:55:24 elastic.jmatica.lan logstash[847]: at org.logstash.FieldReference$StrictTokenizer.tokenize(FieldReference.java:283) ~[logstash-core.jar:?]
May 04 09:55:24 elastic.jmatica.lan logstash[847]: at org.logstash.FieldReference.parse(FieldReference.java:184) ~[logstash-core.jar:?]
May 04 09:55:24 elastic.jmatica.lan logstash[847]: at org.logstash.FieldReference.parseToCache(FieldReference.java:175) ~[logstash-core.jar:?]
May 04 09:55:24 elastic.jmatica.lan logstash[847]: at org.logstash.FieldReference.from(FieldReference.java:107) ~[logstash-core.jar:?]
May 04 09:55:24 elastic.jmatica.lan logstash[847]: at org.logstash.ConvertedMap.put(ConvertedMap.java:75) ~[logstash-core.jar:?]
May 04 09:55:24 elastic.jmatica.lan logstash[847]: at org.logstash.ConvertedMap.newFromMap(ConvertedMap.java:55) ~[logstash-core.jar:?]
May 04 09:55:24 elastic.jmatica.lan logstash[847]: at org.logstash.Valuefier.lambda$initConverters$13(Valuefier.java:152) ~[logstash-core.jar:?]
May 04 09:55:24 elastic.jmatica.lan logstash[847]: at org.logstash.Valuefier.convert(Valuefier.java:74) ~[logstash-core.jar:?]
May 04 09:55:24 elastic.jmatica.lan logstash[847]: at org.logstash.ConvertedMap.newFromMap(ConvertedMap.java:55) ~[logstash-core.jar:?]
May 04 09:55:24 elastic.jmatica.lan logstash[847]: at org.logstash.ext.JrubyEventExtLibrary$RubyEvent.initializeFallback(JrubyEventExtLibrary.java:303) ~[logstash-core.jar:?]
May 04 09:55:24 elastic.jmatica.lan logstash[847]: at org.logstash.ext.JrubyEventExtLibrary$RubyEvent.ruby_initialize(JrubyEventExtLibrary.java:77) ~[logstash-core.jar:?]
May 04 09:55:24 elastic.jmatica.lan logstash[847]: at usr.share.logstash.vendor.bundle.jruby.$2_dot_5_dot_0.gems.logstash_minus_input_minus_beats_minus_6_dot_0_dot_8_minus_java.lib.logstash.inputs.beats.message_listener.RUBY$method$onNewMessage$0(/usr/share/logstash/vendor/bundle/jruby/2.5.0/gems/logstash-input-beats-6.0.8-java/lib/logstash/inputs/beats/message_listener.rb:40) ~[?:?]
May 04 09:55:24 elastic.jmatica.lan logstash[847]: at usr.share.logstash.vendor.bundle.jruby.$2_dot_5_dot_0.gems.logstash_minus_input_minus_beats_minus_6_dot_0_dot_8_minus_java.lib.logstash.inputs.beats.message_listener.RUBY$method$onNewMessage$0$__VARARGS__(/usr/share/logstash/vendor/bundle/jruby/2.5.0/gems/logstash-input-beats-6.0.8-java/lib/logstash/inputs/beats/message_listener.rb) ~[?:?]
May 04 09:55:24 elastic.jmatica.lan logstash[847]: at org.jruby.internal.runtime.methods.CompiledIRMethod.call(CompiledIRMethod.java:84) ~[jruby-complete-9.2.9.0.jar:?]
May 04 09:55:24 elastic.jmatica.lan logstash[847]: at org.jruby.internal.runtime.methods.MixedModeIRMethod.call(MixedModeIRMethod.java:70) ~[jruby-complete-9.2.9.0.jar:?]
May 04 09:55:24 elastic.jmatica.lan logstash[847]: at org.jruby.gen.LogStash$$Inputs$$Beats$$MessageListener_914065315.onNewMessage(org/jruby/gen/LogStash$$Inputs$$Beats$$MessageListener_914065315.gen:13) ~[?:?]
May 04 09:55:24 elastic.jmatica.lan logstash[847]: at org.logstash.beats.BeatsHandler.channelRead0(BeatsHandler.java:52) ~[logstash-input-beats-6.0.8.jar:?]
May 04 09:55:24 elastic.jmatica.lan logstash[847]: at org.logstash.beats.BeatsHandler.channelRead0(BeatsHandler.java:12) ~[logstash-input-beats-6.0.8.jar:?]
May 04 09:55:24 elastic.jmatica.lan logstash[847]: at io.netty.channel.SimpleChannelInboundHandler.channelRead(SimpleChannelInboundHandler.java:105) ~[netty-all-4.1.30.Final.jar:4.1.30.Final]
May 04 09:55:24 elastic.jmatica.lan logstash[847]: at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:362) ~[netty-all-4.1.30.Final.jar:4.1.30.Final]
May 04 09:55:24 elastic.jmatica.lan logstash[847]: at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:348) ~[netty-all-4.1.30.Final.jar:4.1.30.Final]
May 04 09:55:24 elastic.jmatica.lan logstash[847]: at io.netty.channel.AbstractChannelHandlerContext.fireChannelRead(AbstractChannelHandlerContext.java:340) ~[netty-all-4.1.30.Final.jar:4.1.30.Final]
May 04 09:55:24 elastic.jmatica.lan logstash[847]: at io.netty.handler.codec.ByteToMessageDecoder.fireChannelRead(ByteToMessageDecoder.java:323) ~[netty-all-4.1.30.Final.jar:4.1.30.Final]
May 04 09:55:24 elastic.jmatica.lan logstash[847]: at io.netty.handler.codec.ByteToMessageDecoder.channelRead(ByteToMessageDecoder.java:297) ~[netty-all-4.1.30.Final.jar:4.1.30.Final]
May 04 09:55:24 elastic.jmatica.lan logstash[847]: at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:362) ~[netty-all-4.1.30.Final.jar:4.1.30.Final]
May 04 09:55:24 elastic.jmatica.lan logstash[847]: at io.netty.channel.AbstractChannelHandlerContext.access$600(AbstractChannelHandlerContext.java:38) ~[netty-all-4.1.30.Final.jar:4.1.30.Final]
May 04 09:55:24 elastic.jmatica.lan logstash[847]: at io.netty.channel.AbstractChannelHandlerContext$7.run(AbstractChannelHandlerContext.java:353) ~[netty-all-4.1.30.Final.jar:4.1.30.Final]
May 04 09:55:24 elastic.jmatica.lan logstash[847]: at io.netty.util.concurrent.DefaultEventExecutor.run(DefaultEventExecutor.java:66) ~[netty-all-4.1.30.Final.jar:4.1.30.Final]
May 04 09:55:24 elastic.jmatica.lan logstash[847]: at io.netty.util.concurrent.SingleThreadEventExecutor$5.run(SingleThreadEventExecutor.java:897) [netty-all-4.1.30.Final.jar:4.1.30.Final]
May 04 09:55:24 elastic.jmatica.lan logstash[847]: at io.netty.util.concurrent.FastThreadLocalRunnable.run(FastThreadLocalRunnable.java:30) [netty-all-4.1.30.Final.jar:4.1.30.Final]
May 04 09:55:24 elastic.jmatica.lan logstash[847]: at java.lang.Thread.run(Thread.java:748) [?:1.8.0_242]
```

The problem here is that in the log items arriving to LogStash. One solution is:

- find who is sending the offending data to LogStash (if you have multiple beats sending logs)
- stop that filebeat

    ```sh
    $> systemctl stop filebeat
    ```

- remove the lines from the log files:

    ```sh
    $> sed -i "/\bpassword\b/d" <file>
    ```

- remove filebeat registry

    ```sh
    $> rm -rf /var/lib/filebeat/registry
    ```

- restart filebeat

    ```sh
    $> systemctl start filebeat
    ```

The other (and probably the better) solution is to fix logstash pipeline configuration responsible for the offending messages.
