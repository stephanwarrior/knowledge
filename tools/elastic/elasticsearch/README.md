# ElasticSearch

[[_TOC_]]

## Search APIs

see https://www.elastic.co/guide/en/elasticsearch/reference/current/search.html

## Frozen Indexes

See https://www.elastic.co/guide/en/elasticsearch/reference/current/searching_a_frozen_index.html
