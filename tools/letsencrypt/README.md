# Let's Encrypt

In this document we describe how to use the Let's Encrypt `certbot` utility to issue, renew and revoke SSL/TLS certificates.

[[_TOC_]]

## Prerequisites

A webserver responding to the predetermined domain must be already online, publicly accessible on port 80.

For all the following sections, instead of using command line flags and options, it is possible to specify a [configuration file](https://certbot.eff.org/docs/using.html#configuration-file).

## `certbot` Installation

On a CentOS7 system `certbot` can be installed with the following commands:

```bash
$> yum install epel-release
$> yum install certbot python2-certbot-nginx
```

## Certificate issuance

Before running `certbot`, stop your webserver: `certbot` needs to be able to use port `80` to communicate with the Let's Encrypt CA servers.

Then run:

```bash
$> certbot -n certonly --standalone -d $DOMAIN --cert-path /$CERT_PATH/$DOMAIN/fullchain.pem --email $CONTACT_EMAIL --agree-tos
```

This will save certificates and keys under the folders `/etc/letsencrypt/live/$DOMAIN` and `/etc/letsencrypt/archive/$DOMAIN` and also produce a configuration file `/$CERT_PATH/$DOMAIN/fullchain.pem.conf`:

```conf
# renew_before_expiry = 30 days
version = 0.37.2
archive_dir = /etc/letsencrypt/archive/fullchain.pem
cert = /etc/letsencrypt/live/fullchain.pem/cert.pem
privkey = /etc/letsencrypt/live/fullchain.pem/privkey.pem
chain = /etc/letsencrypt/live/fullchain.pem/chain.pem
fullchain = /etc/letsencrypt/live/fullchain.pem/fullchain.pem

# Options used in the renewal process
[renewalparams]
authenticator = standalone
account = 3645ff4a53308bc3baee7b1a8031fc87
server = https://acme-v02.api.letsencrypt.org/directory
```

## Nginx configuration

If at first the nginx configuration was as follows:

```nginx
...
http {
    ...
    server {
      listen 80;
      server_name $DOMAIN;
      root $PATH_TO_APP_ROOT;
      index index.html index.htm;
    }
    ...
}
...
```

Change it to the following:

```nginx
...
http {
    ...
    server {
        if ($host = $DOMAIN) {
            return 301 https://$host$request_uri;
        }

        listen  80;
        server_name $DOMAIN;
        return 404;
    }
    server {
        listen 443 ssl;
        server_name $DOMAIN;
        root $PATH_TO_APP_ROOT;
        index index.html index.htm;

        ssl_certificate /etc/letsencrypt/live/$DOMAIN/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/$DOMAIN/privkey.pem;
        include /etc/letsencrypt/options-ssl-nginx.conf;
        ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;
    }
    ...
}
...
```

Where `/etc/letsencrypt/options-ssl-nginx.conf` is:

```nginx
ssl_session_cache shared:le_nginx_SSL:10m;
ssl_session_timeout 1440m;
ssl_protocols TLSv1.2;
ssl_prefer_server_ciphers off;
ssl_ciphers "ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384";
```

### Automated nginx configuration

It is possible to have `certbot` automatically modify the nginx configuration to use the certificate. To do this it expects it to be installed under the folder `/etc/nginx`.

To do this run:

```bash
$> certbot -n --nginx -d $DOMAIN --cert-path /$CERT_PATH/$DOMAIN/fullchain.pem --email $CONTACT_EMAIL --agree-tos
```

## Certificate renewal

The issued certificates are valid for 3 months. Renewal is possible for any previously-obtained certificates that expire in less than 30 days.

To renew all the certificates present on the system, run the following:

```bash
$> certbot -n renew
```

Preferably this should run as a cron job:

```conf
# check for renewal of all certificates on the system every 12 hours
0 0,12 * * * root certbot renew
```

If you want to renew a single certificate, you can specify its path:

```bash
$> certbot -n renew --cert-path /etc/letsencrypt/live/fullchain.pem
```

### Renewal hooks

The `renew` command includes hooks for running commands or scripts before or after a certificate is renewed. For example, if you have a single certificate obtained using the standalone plugin, you might need to stop the webserver before renewing so standalone can bind to the necessary ports, and then restart it after the plugin is finished. Example:

```bash
$> certbot renew --pre-hook "service nginx stop" --post-hook "service nginx start"
```

## Certificates inspection

To see the certificates currently present on the system, run:

```bash
$> certbot certificates

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Found the following certs:
  Certificate Name: mysite.example.com
    Domains: mysite.example.com
    Expiry Date: 2019-12-24 09:53:35+00:00 (VALID: 89 days)
    Certificate Path: /etc/letsencrypt/live/mysite.example.com/fullchain.pem
    Private Key Path: /etc/letsencrypt/live/mysite.example.com/privkey.pem
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
```

## Certificate revokation

To revoke a certificate run:

```bash
$> certbot revoke --cert-path /etc/letsencrypt/live/$DOMAIN/fullchain.pem
```

---

## Limitations

### Rate limits

Let's Encrypt production servers: See https://letsencrypt.org/docs/rate-limits/

Let's Encrypt staging environment: See https://letsencrypt.org/docs/staging-environment/#rate-limits

### Certificates types

Let’s Encrypt certificates are standard Domain Validation certificates, so you can use them for any server that uses a domain name, like web servers, mail servers, FTP servers, and many more.

Email encryption and code signing require a different type of certificate that Let’s Encrypt does not issue.

### Certificates lifespan

Certificates are valid for 90 days. You can read about why [here](https://letsencrypt.org/2015/11/09/why-90-days.html).

There is no way to adjust this, there are no exceptions. It is recommended automatically renewing your certificates every 60 days.

### Compatibility

See section "Known Incompatible" on this [page](https://letsencrypt.org/docs/certificate-compatibility/).

### Wildcard certificates

To be able to issue and renew wildcard certificates, DNS credentials must be available

---

## Example

```bash
$> certbot -n certonly --standalone -d mysite.example.com --cert-path /root/mysite.example.com/fullchain.pem --email user.name@example.com --agree-tos

Saving debug log to /var/log/letsencrypt/letsencrypt.log
Plugins selected: Authenticator standalone, Installer None
Starting new HTTPS connection (1): acme-v02.api.letsencrypt.org
Obtaining a new certificate

IMPORTANT NOTES:
 - Congratulations! Your certificate and chain have been saved at:
   /etc/letsencrypt/live/mysite.example.com/fullchain.pem
   Your key file has been saved at:
   /etc/letsencrypt/live/mysite.example.com/privkey.pem
   Your cert will expire on 2019-12-24. To obtain a new or tweaked
   version of this certificate in the future, simply run certbot
   again. To non-interactively renew *all* of your certificates, run
   "certbot renew"
 - If you like Certbot, please consider supporting our work by:

   Donating to ISRG / Let's Encrypt:   https://letsencrypt.org/donate
   Donating to EFF:                    https://eff.org/donate-le
```

---

## Links and Resources

- [Main website](https://www.sonarqube.org/)
