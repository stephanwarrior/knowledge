# Ansible AWX

[[_TOC_]]

## Glossary

**Inventories** are groups of host servers that managed by Ansible AWX.

**Credentials** are used to access the hosts listed in the inventories. Simply, the 'Machine' credential will allow you to use the SSH authentication for managing servers. Its support for both password and key-based authentications.

**Projects** are represented as Ansible Playbooks on the AWX. Its collections of Ansible Playbooks that we can manage through local project directory or using the SCM system such as Git, Subversion, Mercurial, and RedHat Insights.

A **job template** is the definition of running Ansible playbooks itself. So, in order to create a new job template or run the job template, we need to add Ansible playbook from our 'Project', the 'Credentials' for authentication, and the target machines that stored on the 'Inventories'.

## Installation

Here follow the instructions for the installation of [AWX](https://github.com/ansible/awx).

We will run it using the [`docker-compose`](https://github.com/ansible/awx/blob/devel/INSTALL.md#docker-compose) method.

### Prerequisites

We install `git` to retrieve the project's source code and `epel-release` for the latest version of Ansible. Also, make sure that `make` is present on the host.

```bash
$> yum update
$> yum install -y epel-release git
$> yum makecache
$> git clone https://github.com/ansible/awx.git
```

### Dependencies

#### Ansible

Install [Ansible](https://www.ansible.com/).

```bash
$> yum install -y ansible
```

#### Docker

Install [Docker](https://www.docker.com/).

```bash
$> curl -fsSL https://get.docker.com -o get-docker.sh
$> sh get-docker.sh
$> systemctl enable docker
$> systemctl start docker
```

#### `docker` python module

Install the `docker` [python module](https://pypi.org/project/docker/).

```bash
$> yum install python-pip python-devel
$> pip install docker
```

#### Node.js

Install [Node.js](https://nodejs.org/) and `npm`.

```bash
$> curl -sL https://rpm.nodesource.com/setup_10.x | bash -
$> yum install -y nodejs gcc-c++ make
```

#### Docker-compose

Install [Docker compose](https://docs.docker.com/compose/).

```bash
$> curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
$> chmod +x /usr/local/bin/docker-compose
$> ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
$> curl -L https://raw.githubusercontent.com/docker/compose/1.24.1/contrib/completion/bash/docker-compose -o /etc/bash_completion.d/docker-compose
```

#### `docker-compose` python module

Install the `docker-compose` [python module](https://pypi.org/project/docker-compose/).

```bash
$> pip install docker-compose
```

### Install AWX

We can now proceed with the installation.

```bash
$> cd awx/installer
$> ansible-playbook -i inventory install.yml
```

At the end of the process we'll see 5 Docker containers (check with `docker ps`). At this point we will have to wait some more, because they will perform some operations necessary at the first run for the system to be ready.

### Use AWX

Once all is done, go to `http://<machine-IP-or-address>` and browse.

Default credentials are `admin`/`password`.

---

## Links and Resources

- [Main website](https://www.sonarqube.org/)
